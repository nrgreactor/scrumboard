﻿using ScrumBoard.DAL;
using ScrumBoard.DAL.Repositories;
using ScrumBoard.DM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumBoard.BLL.Services
{
    public class ProjectsService
    {
        public ObservableCollection<ProjectDataModel> GetAllProjectMethod(UserDataModel User)
        {
            ObservableCollection<ProjectDataModel> ProjectsCollection = new ObservableCollection<ProjectDataModel>();
            try
            {
                var rep = new ProjectRepository();
                foreach (var item in rep.GetProjectsByUserId(User.Id))
                {
                    ProjectsCollection.Add(item);
                }
            }
            catch
            {
                throw new EntityAlreadyExistsException();
            }
            return ProjectsCollection;
        }

        public string AddUserMethod(UserDataModel User, ProjectDataModel _selectedProject, UserDataModel CurrentUser)
        {
            string answer;
            UserToProjectRepository rep = new UserToProjectRepository();
            UserToProjectDataModel role = rep.GetByProjectId(CurrentUser, _selectedProject);
            if (role.Role.Trim().Equals("Master"))
            {
                try
                {
                    UserRepository usRep = new UserRepository();
                    UserDataModel ThisUser = usRep.GetByNickname(User.Login);
                    UserToProjectDataModel userTo = new UserToProjectDataModel
                    {
                        UserId = ThisUser.Id,
                        ProjectId = _selectedProject.Id,
                        Role = "Developer"
                    };
                    rep.Add(userTo);
                    answer = "User " + User.Login.Trim() + " was added";
                    return answer;
                }
                catch
                {
                    answer = "Cannot find this user";
                    return answer;
                }
            }
            else
            {
                answer = "You aren't master in this project";
                return answer;
            }
        }

        public string RemoveUserMethod(UserDataModel User, ProjectDataModel _selectedProject, UserDataModel CurrentUser)
        {

            string answer;
            UserToProjectRepository rep = new UserToProjectRepository();
            UserToProjectDataModel role = rep.GetByProjectId(CurrentUser, _selectedProject);
            if (role.Role.Trim().Equals("Master"))
            {
                try
                {
                    rep.Delete(_selectedProject, User);
                    answer = "User " + User.Login.Trim() + " was deleted";
                    return answer;
                }
                catch
                {
                    answer = "Cannot delete user";
                    return answer;
                }
            }
            else
            {
                answer = "You aren't master in this project";
                return answer;
            }
        }

        public ObservableCollection<UserDataModel> ViewUsersMethod(ProjectDataModel SelectedProject)
        {
            ObservableCollection<UserDataModel> UsersCollection = new ObservableCollection<UserDataModel>();
            if (SelectedProject != null)
            {
                try
                {
                    var rep = new UserRepository();
                    foreach (var item in rep.GetByProjectID(SelectedProject.Id))
                    {
                        UsersCollection.Add(item);
                    }
                }
                catch
                {
                    throw new EntityAlreadyExistsException();
                }
            }
            return UsersCollection;
        }

        public void AddProjectMethod(ProjectDataModel Project, UserToProjectDataModel UserToProject)
        {
            try
            {
                var rep = new ProjectRepository();
                Project.DateStart = DateTime.Now;
                Project.Status = "Active";
                UserToProject.Role = "Master";
                rep.Add(Project, UserToProject);
            }
            catch
            {
                throw new EntityAlreadyExistsException();
            }
        }

        public string DeleteProjectMethod(ProjectDataModel _selectedProject, UserDataModel CurrentUser)
        {
            string answer;
            UserToProjectRepository rep = new UserToProjectRepository();
            UserToProjectDataModel role = rep.GetByProjectId(CurrentUser, _selectedProject);
            if (role.Role.Trim().Equals("Master"))
            {
                try
                {
                    ProjectRepository prRep = new ProjectRepository();
                    rep.Delete(_selectedProject, CurrentUser);
                    prRep.Delete(_selectedProject);
                    answer = "Project " + _selectedProject.Name.Trim() + " was deleted";
                    return answer;
                }
                catch
                {
                    answer = "Cannot delete the project";
                    return answer;
                }
            }
            else
            {
                answer = "You aren't master in this project";
                return answer;
            }
        }
    }
}
