﻿using ScrumBoard.DAL.Repositories;
using ScrumBoard.DM;


namespace ScrumBoard.BLL.Services
{
    public class SignUpService
    {
        public bool SignUp(UserDataModel user)
        {
                UserRepository userRepo = new UserRepository();
                user.Password = new LogInService().GetHashString(user.Password);
                userRepo.Add(user);
                return true;
        }
    }
}
