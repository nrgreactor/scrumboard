﻿using ScrumBoard.DAL.Repositories;
using ScrumBoard.DM;

namespace ScrumBoard.BLL.Services
{
    public class UserSettingsService
    {
        private UserRepository r = new UserRepository();
        public void Update(UserDataModel user)
        {
            r.Update(user);
        }
    }
}
