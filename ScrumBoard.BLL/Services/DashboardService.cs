﻿using ScrumBoard.DAL.Repositories;
using ScrumBoard.DM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumBoard.BLL.Services
{
    public class DashboardService
    {
        private TaskRepository tr = new TaskRepository();
        public ObservableCollection<TaskDataModel> GetSprintTasks(SprintDataModel sprint)
        {
            var taskCollection = new ObservableCollection<TaskDataModel>();
            foreach (var item in tr.GetBySprintId(sprint.Id))
            {
                taskCollection.Add(item);
            }
            return taskCollection;
        }
        public void SetSprintTask(TaskDataModel task)
        {
            tr.Update(task);
        }
        public ObservableCollection<TaskDataModel> GetStoryTasks(TaskDataModel task)
        {
            
                var taskCollection = new ObservableCollection<TaskDataModel>();
                List<TaskDataModel> list=(List<TaskDataModel>)tr.GetByStoryId(task.Id);
                foreach (var item in list)
                {
                    taskCollection.Add(item);
                }
                return taskCollection;
            
        }

        
    }
}
