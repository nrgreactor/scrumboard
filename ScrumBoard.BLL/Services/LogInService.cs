﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScrumBoard.DM;
using ScrumBoard.DAL.Repositories;
using ScrumBoard.DAL;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using AutoMapper;

namespace ScrumBoard.BLL.Services
{
    public class LogInService
    {
        private UserDataModel user;
        public string FilePath { get; set; }
        public LogInService()
        {
            FilePath = @"remember.bin";
        }
        //private bool LogInResult = false;
        public bool LogIn(string nickname, string password, bool doRemember)
        {
            UserRepository _UserRepo = new UserRepository();

            this.user = _UserRepo.GetByNickname(nickname);

            if (GetHashString(password) == user.Password)
            {
                //LogInResult = true;
                SetRemember(doRemember);
                return true;
            }
            else
            {
                throw new AuthenticationFailedException("Wrong password");
            }
        }

        public void LogOut()
        {
            File.Delete(FilePath);
        }
        //запускать только после LogIn
        public UserDataModel GetUser()
        {
            if (user != null)
                return this.user;
            else throw new EntityNotFoundException("User not found");
        }

        /// <summary>
        /// Метод шифрования строки.
        /// </summary>
        public string GetHashString(string s)
        {
            //переводим строку в байт-массим  
            byte[] bytes = Encoding.Unicode.GetBytes(s);

            //создаем объект для получения средст шифрования  
            MD5CryptoServiceProvider CSP =
                new MD5CryptoServiceProvider();

            //вычисляем хеш-представление в байтах  
            byte[] byteHash = CSP.ComputeHash(bytes);

            string hash = string.Empty;

            //формируем одну цельную строку из массива  
            foreach (byte b in byteHash)
                hash += string.Format("{0:x2}", b);

            return hash;
        }

        /// <summary>
        /// Метод проверки, существует ли файл сохраненного пользователя и возвращает его
        /// </summary>
        private void SetRemember(bool rem)
        {
            if (rem == true)
                using (var fs = new FileStream(FilePath, FileMode.Create))
                {
                    var bf = new BinaryFormatter();
                    bf.Serialize(fs, this.user);
                }
        }



        /// <summary>
        /// Метод сохранния текущего пользователя в файл.
        /// </summary>
        public UserDataModel GetRemember()
        {
            if (File.Exists(FilePath))
                using (var fs = new FileStream(FilePath, FileMode.Open))
                {
                    var bf = new BinaryFormatter();
                    return user = (UserDataModel)bf.Deserialize(fs);
                }
            else return null;
        }
        

    }
}
