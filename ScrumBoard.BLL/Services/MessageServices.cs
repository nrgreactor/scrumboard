﻿using ScrumBoard.DAL;
using ScrumBoard.DAL.Repositories;
using ScrumBoard.DM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumBoard.BLL.Services
{
    public class MessageServices
    {
        public ObservableCollection<MessageDataModel> ViewAllTasksMethod(TaskDataModel CurrentTask)
        {

            ObservableCollection<MessageDataModel> MessagesCollection = new ObservableCollection<MessageDataModel>();
            try
            {
                var rep = new MessageRepository();
                foreach (var item in rep.GetMessagesByTaskId(CurrentTask.Id))
                {
                    MessagesCollection.Add(item);
                }
            }
            catch
            {
                throw new EntityAlreadyExistsException();
            }
            return MessagesCollection;
        }

        public void AddMessageMethod(TaskDataModel CurrentTask, UserDataModel CurrentUser, MessageDataModel Message)
        {
            try
            {
                var rep = new MessageRepository();
                Message.TaskId = CurrentTask.Id;
                Message.UserId = CurrentUser.Id;
                rep.Add(Message);
            }
            catch
            {
                throw new EntityAlreadyExistsException();
            }
        }
    }
}
