﻿using ScrumBoard.DAL.Repositories;
using ScrumBoard.DM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumBoard.BLL.Services
{
    public class BacklogService
    {
        private TaskRepository r = new TaskRepository();
        public void AddTask(TaskDataModel task)
        {            
            r.Add(task);
        }
        public ObservableCollection<TaskDataModel> GetTasks(ProjectDataModel project)
        {
            var obsList = new ObservableCollection<TaskDataModel>();
            
            foreach(var item in r.GetByProjectId(project.Id).Reverse()){
                obsList.Add(item);
            }
            return obsList;
        }
        public void UpdateTask(TaskDataModel task)
        {
            r.Update(task);
        }
        public void DeleteTask(TaskDataModel task)
        {
            r.Delete(task);
        }
    }
}
