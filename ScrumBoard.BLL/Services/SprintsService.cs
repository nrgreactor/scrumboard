﻿using ScrumBoard.DAL;
using ScrumBoard.DAL.Repositories;
using ScrumBoard.DM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumBoard.BLL.Services
{
    public class SprintsService
    {
        public ObservableCollection<TaskDataModel> ViewAllTasksMethod(ProjectDataModel CurrentProject)
        {

            ObservableCollection<TaskDataModel> ProjectTasksCollection = new ObservableCollection<TaskDataModel>();
            try
            {

                var rep = new TaskRepository();
                foreach (var item in rep.GetByProjectId(CurrentProject.Id))
                {
                    if (item.SprintId == null)
                    {
                        ProjectTasksCollection.Add(item);
                    }
                }
            }
            catch
            {
                throw new EntityAlreadyExistsException();
            }
            return ProjectTasksCollection;
        }

        public void AddTaskMethod(SprintDataModel CurrentSprint, TaskDataModel _selectedTask)
        {
            try
            {
                _selectedTask.Status = 0;
                TaskRepository rep = new TaskRepository();
                _selectedTask.SprintId = CurrentSprint.Id;
                rep.Update(_selectedTask);
            }
            catch
            {
                throw new EntityAlreadyExistsException();
            }
        }

        public void RemoveTaskMethod(TaskDataModel _selectedTask)
        {
            try
            {
                _selectedTask.Status = 0;
                TaskRepository rep = new TaskRepository();
                _selectedTask.SprintId = null;
                rep.Update(_selectedTask);
            }
            catch
            {
                throw new EntityAlreadyExistsException();
            }

        }

        public ObservableCollection<TaskDataModel> ViewSprintTasksMethod(SprintDataModel CurrentSprint)
        {
            ObservableCollection<TaskDataModel> SprintTasksCollection = new ObservableCollection<TaskDataModel>();
            try
            {

                var rep = new TaskRepository();
                foreach (var item in rep.GetBySprintId(CurrentSprint.Id))
                {
                    SprintTasksCollection.Add(item);
                }

            }
            catch
            {
                throw new EntityAlreadyExistsException();
            }
            return SprintTasksCollection;
        }

        public ObservableCollection<SprintDataModel> ViewAllSprintsMethod(ProjectDataModel Project)
        {
            ObservableCollection<SprintDataModel> SprintsCollection = new ObservableCollection<SprintDataModel>();
            try
            {

                var rep = new SprintRepository();
                foreach (var item in rep.GetByProjectId(Project.Id))
                {
                    SprintsCollection.Add(item);
                }

            }
            catch
            {
                throw new EntityAlreadyExistsException();
            }
            return SprintsCollection;
        }

        public void AddSprintMethod(SprintDataModel Sprint, ProjectDataModel Project)
        {
            try
            {
                SprintRepository rep = new SprintRepository();
                Sprint.ProjectId = Project.Id;
                Sprint.DateStart = DateTime.Today;
                Sprint.DateEnd = DateTime.Today;
                rep.Add(Sprint);
            }
            catch
            {
                throw new EntityAlreadyExistsException();
            }
        }

        public void SetSprintDateMethod(SprintDataModel Sprint)
        {
            try
            {
                SprintRepository rep = new SprintRepository();
                rep.Update(Sprint);
            }
            catch
            {
                throw new EntityAlreadyExistsException();
            }
        }

        public string DeleteSprintMethod(ProjectDataModel _selectedProject, UserDataModel CurrentUser, SprintDataModel _selectedSprint)
        {
            string answer;
            UserToProjectRepository rep = new UserToProjectRepository();
            UserToProjectDataModel role = rep.GetByProjectId(CurrentUser, _selectedProject);
            if (role.Role.Trim().Equals("Master"))
            {
                try
                {
                    SprintRepository spRep = new SprintRepository();
                    spRep.Delete(_selectedSprint);
                    answer = "Sprint " + _selectedSprint.Name.Trim() + " was deleted";
                    return answer;
                }
                catch
                {
                    answer = "Cannot delete the sprint";
                    return answer;
                }
            }
            else
            {
                answer = "You aren't master in this project";
                return answer;
            }
        }
    }
}
