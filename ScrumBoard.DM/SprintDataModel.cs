﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumBoard.DM
{
    public class SprintDataModel : IDataErrorInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> DateStart { get; set; }
        public Nullable<System.DateTime> DateEnd { get; set; }
        public int ProjectId { get; set; }
        public Nullable<int> Time { get; set; }
        public Nullable<int> Status { get; set; }

        #region IDataErrorInfoMembers

        string IDataErrorInfo.this[string propertyName]
        {

            get
            {
                return GetValidationError(propertyName);
            }
        }

        string IDataErrorInfo.Error
        {
            get
            {
                return null;
            }
        }


        #endregion

        #region Validation

        static readonly string[] ValidatedProperties = 
        {
            "Name"
        };

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationError(property) != null)
                        return false;
                return true;
            }
        }

        private string ValidateName()
        {
            if (Name.Length < 4)
            {
                return "Enter name";
            }
            return null;
        }


        string GetValidationError(String propertyName)
        {
            string error = null;

            switch (propertyName)
            {
                case "Name":
                    error = ValidateName();
                    break;
            }
            return error;
        }
        #endregion
    }
}
