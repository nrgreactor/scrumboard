﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ScrumBoard.DM
{
    [Serializable]
    public class UserDataModel : IDataErrorInfo
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public byte[] Photo { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        #region IDataErrorInfoMembers

        string IDataErrorInfo.this[string propertyName]
        {

            get
            {
                return GetValidationError(propertyName);
            }
        }

        string IDataErrorInfo.Error
        {
            get
            {
                return null;
            }
        }


        #endregion

        #region Validation

        static readonly string[] ValidatedProperties = 
        {
            "Login",
            "FirstName",
            "LastName",
            "Email",
            "Password"
        };

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationError(property) != null)
                        return false;
                return true;
            }
        }

        private string ValidateFirstName()
        {
            if (FirstName.Length < 3)
            {
                return "Enter name";
            }
            if (!Char.IsUpper(FirstName[0]))
            {
                var g = Char.IsUpper(FirstName[0]);
                return "First letter must be in upper case";
            }
            return null;
        }

        private string ValidateLastName()
        {
            if (LastName.Length < 3)
            {
                return "Enter name";
            }
            if (!Char.IsUpper(LastName[0]))
            {
                return "First letter must be in upper case";
            }
            return null;
        }

        private string ValidateLogin()
        {
            if (Login.Length < 5)
            {
                return "Enter name not less than 5 symbols";
            }
            if (!Regex.IsMatch(Login, @"^[a-zA-Z0-9]+$"))
            {
                return "Login can have only letters and numbers";
            }
            return null;
        }

        private string ValidateEmail()
        {
            if (Email.Length < 8)
            {
                return "Enter email";
            }
            try
            {
                var mail = new System.Net.Mail.MailAddress(Email);
            }
            catch
            {
                return "Not corrected email";
            }

            return null;
        }


        private string ValidatePassword()
        {
            if (Password.Length < 8)
            {
                return "Password must be not less than 8 symbols";
            }
            return null;
        }


        public string GetValidationError(String propertyName)
        {
            string error = null;

            switch (propertyName)
            {
                case "Login":
                    error = ValidateLogin();
                    break;
                case "Password":
                    error = ValidatePassword();
                    break;
                case "FirstName":
                    error = ValidateFirstName();
                    break;
                case "LastName":
                    error = ValidateLastName();
                    break;
                case "Email":
                    error = ValidateEmail();
                    break;

            }
            return error;
        }
        #endregion
    }
}

