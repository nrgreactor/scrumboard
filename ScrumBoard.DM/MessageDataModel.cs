﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumBoard.DM
{
    public class MessageDataModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Time { get; set; }
        public int TaskId { get; set; }
        public int UserId { get; set; }
        public string FullName { get { return String.Format("{0} {1}",User.FirstName, User.LastName); } }

        public virtual TaskDataModel Task { get; set; }
        public virtual UserDataModel User { get; set; }
    }
}
