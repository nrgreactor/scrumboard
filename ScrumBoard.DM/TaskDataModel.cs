﻿using System;
using System.ComponentModel;

namespace ScrumBoard.DM
{
    public class TaskDataModel : IDataErrorInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Descriprion { get; set; }
        public Nullable<int> Estimate { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> ProjectId { get; set; }
        public Nullable<int> SprintId { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<System.DateTime> DateStart { get; set; }
        public Nullable<System.DateTime> DateEnd { get; set; }
        public Nullable<int> StoryId { get; set; }
        public Nullable<int> Time { get; set; }
        public virtual UserDataModel User { get; set; }

        #region IDataErrorInfoMembers

        string IDataErrorInfo.this[string propertyName]
        {

            get
            {
                return GetValidationError(propertyName);
            }
        }

        string IDataErrorInfo.Error
        {
            get
            {
                return null;
            }
        }


        #endregion

        #region Validation

        static readonly string[] ValidatedProperties = 
        {
            "Name",
            "Estimate"
        };

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                    if (GetValidationError(property) != null)
                        return false;
                return true;
            }
        }

        private string ValidateName()
        {
            if (Name.Length < 3)
            {
                return "Enter name";
            }
            return null;
        }

        private string ValidateEstimate()
        {
            if (Estimate == 0 || Estimate == null)
            {
                return "Enter estimate";
            }
            return null;
        }


        string GetValidationError(String propertyName)
        {
            string error = null;

            switch (propertyName)
            {
                case "Name":
                    error = ValidateName();
                    break;
                case "Estimate":
                    error = ValidateEstimate();
                    break;
            }
            return error;
        }
        #endregion
    }
}
