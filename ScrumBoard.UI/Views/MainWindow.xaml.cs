﻿using ScrumBoard.UI.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ScrumBoard.UI.ViewModels;
using ScrumBoard.BLL.Services;
using Framework.UI.Controls;
using ScrumBoard.UI.Navigation;

namespace ScrumBoard.UI.Views
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void mainFrameLoaded(object sender, RoutedEventArgs e)
        {
            
            var service = new LogInService();
            if (service.GetRemember() == null)
            {
                PageNavigator.Instance.Navigate(ViewManager.Instance.AuthenticationView());
            }
            else
            {
                ViewManager.Instance.User = service.GetUser();
                PageNavigator.Instance.Navigate(ViewManager.Instance.UpperView());
            }
        }       
    }
}
