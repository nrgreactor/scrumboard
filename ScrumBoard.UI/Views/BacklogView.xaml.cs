﻿using System.Windows;
using System.Windows.Controls;

namespace ScrumBoard.UI.Views
{
    /// <summary>
    /// Логика взаимодействия для BacklogView.xaml
    /// </summary>
    public partial class BacklogView : Page
    {
        public BacklogView()
        {
            InitializeComponent();
        }

        private void Cancel_Add(object sender, RoutedEventArgs e)
        {
            item2.IsSelected = true;
        }

        private void SelectItem(object sender, SelectionChangedEventArgs e)
        {
            item2.IsSelected = true;
        }
    }
}
