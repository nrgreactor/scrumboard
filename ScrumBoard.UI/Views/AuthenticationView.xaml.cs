﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScrumBoard.UI.Views
{
    /// <summary>
    /// Логика взаимодействия для AuthenticationView.xaml
    /// </summary>
    public partial class AuthenticationView : Page
    {
        public AuthenticationView(LogInView loginInView, SignUpView signUpView)
        {
            InitializeComponent();
            logInFrame.Navigate(loginInView);
            SignUpFrame.Navigate(signUpView);            
        }

        private void AccordionItem_Selected(object sender, RoutedEventArgs e)
        {
            Application.Current.Windows.OfType<Views.MainWindow>().Single().MinHeight = 520;
            Application.Current.Windows.OfType<Views.MainWindow>().Single().MinWidth = 583;
        }

        private void AccordionItem_Selected_1(object sender, RoutedEventArgs e)
        {
            Application.Current.Windows.OfType<Views.MainWindow>().Single().MinHeight = 420;
            Application.Current.Windows.OfType<Views.MainWindow>().Single().MinWidth = 400;
        }
    }
}
