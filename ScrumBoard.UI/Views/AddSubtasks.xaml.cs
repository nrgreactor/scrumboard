﻿using ScrumBoard.DM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScrumBoard.UI.Views
{
    /// <summary>
    /// Логика взаимодействия для AddSubtasks.xaml
    /// </summary>
    public partial class AddSubtasks
    {
        private List<TaskDataModel> _allTasks;        
        public List<TaskDataModel> AllTasks
        {
            get { return _allTasks; }
            set
            {
                _allTasks = value;
            }
        }
        public TaskDataModel SelTask;
        public AddSubtasks()
        {
            InitializeComponent();
            AllTasks = new List<TaskDataModel>();
            this.DataContext = this;
        }  private void AddBT_Click(object sender, RoutedEventArgs e)
        {
            if (LBTasksAll.SelectedIndex!=-1)
            SelTask = (TaskDataModel)LBTasksAll.SelectedItem; 
            this.Hide();          
        }    
    }
}
