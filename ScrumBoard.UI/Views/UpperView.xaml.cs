﻿using ScrumBoard.UI.Navigation;
using ScrumBoard.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScrumBoard.UI.Views
{
    /// <summary>
    /// Логика взаимодействия для UpperView.xaml
    /// </summary>
    public partial class UpperView : Page
    {
        public UpperView()
        {
            InitializeComponent();

            Application.Current.Windows.OfType<Views.MainWindow>().Single().MinHeight = 600;
            Application.Current.Windows.OfType<Views.MainWindow>().Single().MinWidth = 800;
            upperViewFrame.Navigate(ViewManager.Instance.ProjectsView());
            SettingsExpander.IsExpanded = false;
        }
      
        private void projectsTab_MouseUp(object sender, MouseButtonEventArgs e)
        {
            upperViewFrame.Navigate(ViewManager.Instance.ProjectsView());
        }

        private void backlogTab_MouseUp(object sender, MouseButtonEventArgs e)
        {
            upperViewFrame.Navigate(ViewManager.Instance.BacklogView());
        }

        private void SettingsExpander_LostFocus(object sender, RoutedEventArgs e)
        {
            SettingsExpander.IsExpanded = false;
        }
        private void LogOut_Click(object sender, RoutedEventArgs e)
        {
            new ScrumBoard.BLL.Services.LogInService().LogOut();
            PageNavigator.Instance.Navigate(ViewManager.Instance.AuthenticationView());
        }
        private void UserSettigs_Click(object sender, RoutedEventArgs e)
        {
            PageNavigator.Instance.Navigate(ViewManager.Instance.UserSettingsView());
        }

        private void sprintTab_MouseUp(object sender, MouseButtonEventArgs e)
        {
            upperViewFrame.Navigate(ViewManager.Instance.SprintsView());
        }
        private void dashboardTab_MouseUp(object sender, MouseButtonEventArgs e)
        {
            upperViewFrame.Navigate(ViewManager.Instance.DashboardView());
        }
    }
}
