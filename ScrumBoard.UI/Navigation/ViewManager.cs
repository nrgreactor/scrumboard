﻿using System;
using ScrumBoard.UI.ViewModels;
using ScrumBoard.DM;
using System.Threading;
using Framework.UI.Controls;
using ScrumBoard.UI.Views;
namespace ScrumBoard.UI.Navigation
{
    //
    //Using pattern Singleton
    //вызов происходит следующим образом ViewManager.Instatce.[нужный обьект]
    public sealed class ViewManager
    {
        #region ImplementPattern

        private static readonly Object s_lock = new Object();
        private static ViewManager instance = null;
        public static ViewManager Instance
        {
            get
            {
                if (instance != null) return instance;
                Monitor.Enter(s_lock);
                ViewManager temp = new ViewManager();
                Interlocked.Exchange(ref instance, temp);
                Monitor.Exit(s_lock);
                return instance;
            }
        }
        #endregion
        private ViewManager()
        {

        }
        public UserDataModel User { get; set; }
        //текущий проект (выбраный) хранится здесь
        public ProjectDataModel Project { get; set; }        
        public SprintDataModel Sprint { get; set; }

        private ProjectsView projectsView = new ProjectsView();
        private BacklogView backlogView = new BacklogView();
        private SprintsView sprintsView = new SprintsView();
        private DashboardView dashboardView = new DashboardView();
        private UserSettingsView userSettingsView = new UserSettingsView();

        public AuthenticationView AuthenticationView()
        {
            var logInView = new LogInView() { DataContext = new LogInViewModel() };
            var signUpView = new SignUpView() { DataContext = new SignUpViewModel() };
            return new AuthenticationView(logInView, signUpView);
        }
        public ProjectsView ProjectsView()
        {
            projectsView.DataContext = new ProjectsViewModel(User) ;
            return projectsView;
        }
        public UpperView UpperView()
        {
            return new UpperView() { DataContext=User};
        }
        public BacklogView BacklogView()
        {
            if(this.Project!=null){
            backlogView.DataContext = new BacklogViewModel(Project, User);
            return backlogView;}
            else { MessageDialog.ShowAsync("Project wasn't selected.", null); return null; }
        }
        public SprintsView SprintsView()
        {
            if (this.Project != null)
            {
                sprintsView.DataContext = new SprintsViewModel(Project, User) ;
                return sprintsView;
            }
            else { MessageDialog.ShowAsync("Project wasn't selected.", null); return null; }
        
        }
        public DashboardView DashboardView()
        {
            if (this.Project != null)
            {
                dashboardView.DataContext = new DashboardViewModel(Project);
                return dashboardView;
            }
            else { MessageDialog.ShowAsync("Project wasn't selected.", null); return null; }        
        }
        public UserSettingsView UserSettingsView()
        {
            userSettingsView.DataContext = new UserSettingsViewModel();
            return userSettingsView;
        }
    }
}
