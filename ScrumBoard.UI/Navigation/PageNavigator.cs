﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ScrumBoard.UI.Navigation
{
    public sealed class PageNavigator    
    {
        #region ImplementPattern

        private static readonly Object s_lock = new Object();
        private static PageNavigator instance = null;
        public static PageNavigator Instance
        {
            get
            {
                if (instance != null) return instance;
                Monitor.Enter(s_lock);
                PageNavigator temp = new PageNavigator();
                Interlocked.Exchange(ref instance, temp);
                Monitor.Exit(s_lock);
                return instance;
            }
        }
        #endregion
        private Frame MainFrame;
        private PageNavigator()
        {
            MainFrame = Application.Current.Windows.OfType<Views.MainWindow>().Single().mainFrame;
        }
        public void Navigate(Page page)
        {
            MainFrame.Navigate(page);
        }

    }
}
