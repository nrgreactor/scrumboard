﻿using System;
using System.Windows.Input;
using ScrumBoard.DM;
using ScrumBoard.BLL.Services;
using ScrumBoard.UI.Views;
using ScrumBoard.UI.Navigation;


namespace ScrumBoard.UI.ViewModels
{
    class LogInViewModel : PropertyChangedBase
    {
        public LogInViewModel()
        {
            LogInCommand = new Command(arg => LogInMethod());            
            Password = String.Empty;
            Nickname = String.Empty;
            IfRemember = false;
            service = new LogInService();            
        }
        public string Password { get; set; }
        public string Nickname { get; set; }
        public bool IfRemember { get; set; }
        public ICommand LogInCommand { get; set; }
        private LogInService service;

        private void LogInMethod()
        {
            try
            {
                if (service.LogIn(Nickname, Password, IfRemember))
                {
                    logInSuccesfull(service.GetUser());
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }
        public void logInSuccesfull(UserDataModel user)
        {
            ViewManager.Instance.User = user;
            PageNavigator.Instance.Navigate(ViewManager.Instance.UpperView());
        }

    }
}
