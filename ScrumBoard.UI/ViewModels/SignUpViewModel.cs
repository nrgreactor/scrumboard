﻿using ScrumBoard.BLL.Services;
using ScrumBoard.DM;
using System;
using System.IO;
using System.Windows.Input;
using System.Windows.Media;

namespace ScrumBoard.UI.ViewModels
{
    class SignUpViewModel : PropertyChangedBase
    {

        public SignUpViewModel()
        {
            SignUpCommand = new Command(arg => SignUpMethod());
            LoadImageCommand = new Command(arg => LoadImageMethod());
            Login = String.Empty;
            FirstName = String.Empty;
            LastName = String.Empty;
            Email = String.Empty;
            Password = String.Empty;
            _photo = null;
        }
        #region FieldsAndProperties
        private string _login;
        private string _firstName;
        private string _lastName;
        private ImageSource _photoImage;
        private string _email;
        private string _password;
        private byte[] _photo;

        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                OnPropertyChanged();
            }
        }
        public string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                OnPropertyChanged();
            }
        }
        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                OnPropertyChanged();
            }
        }
        public ImageSource PhotoImage
        {
            get { return _photoImage; }
            set
            {
                _photoImage = value;
                OnPropertyChanged();
            }
        }
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                OnPropertyChanged();
            }
        }
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        public ICommand SignUpCommand { get; set; }
        public ICommand LoadImageCommand { get; set; }
        #endregion
        private void SignUpMethod()
        {
            try
            {
                var service = new SignUpService();

                var user = new UserDataModel();
                user.Login = this.Login;
                user.FirstName = this.FirstName;
                user.LastName = this.LastName;
                user.Photo = this._photo;
                user.Email = this.Email;
                user.Password = this.Password;
                if (user.IsValid)
                {

                    if (service.SignUp(user))
                    {
                        ShowMessage("New user registration successful");
                    }
                }
                else
                    ShowMessage("Check your input values");
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        private void LoadImageMethod()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "All Images |*.jpg;*.jpeg;*.png;*.gif|JPG Files (*.jpg)|*.jpg|JPEG Files (*.jpeg)|*jpeg|PNG Files (*.png)|*.png|GIF Files (*.gif)|*.gif";


            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = @dlg.FileName;
                this._photo = File.ReadAllBytes(@filename);
            }
            UpdateImage();
        }
        private void UpdateImage()
        {
            if (_photo != null)
            {
                ImageSourceConverter converter = new ImageSourceConverter();
                ImageSource imageSource = (ImageSource)converter.ConvertFrom(_photo);
                PhotoImage = imageSource;
            }
        }



    }
}
