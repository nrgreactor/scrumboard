﻿using ScrumBoard.DM;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using ScrumBoard.UI.Views;
using ScrumBoard.BLL.Services;
using ScrumBoard.UI.Navigation;

namespace ScrumBoard.UI.ViewModels
{
    class ProjectsViewModel : PropertyChangedBase
    {
        #region Constructor
        public ProjectsViewModel(UserDataModel _user)
        {
            Project = new ProjectDataModel
            {
                DateEnd = DateTime.Now,
                Name = "New Project"
            };
            UserToAdd = new UserDataModel();
            ViewProjectCommand = new Command(arg => GetAllProjectMethod());
            User = _user;
            UserToProject = new UserToProjectDataModel
            {
                UserId = _user.Id
            };

            AddUserCommand = new Command(arg => AddUserMethod());
            RemoveUserCommand = new Command(arg => RemoveUserMethod());
            ViewUsersCommand = new Command(arg => ViewUsersMethod());
            AddProjectCommand = new Command(arg => AddProjectMethod());
            DeleteProjectCommand = new Command(arg => DeleteProjectMethod());

            Service = new ProjectsService();
            GetAllProjectMethod();
        }
        #endregion Constructor

        #region Commands
        public ICommand ViewProjectCommand { get; set; }
        public ICommand AddUserCommand { get; set; }
        public ICommand RemoveUserCommand { get; set; }
        public ICommand ViewUsersCommand { get; set; }
        public ICommand AddProjectCommand { get; set; }
        public ICommand DeleteProjectCommand { get; set; }
        #endregion Commands

        #region Properties
        private ProjectsService Service;
        //Private fields
        private ProjectDataModel _project;
        private UserDataModel _selectedUser;
        private ProjectDataModel _selectedProject;
        private ObservableCollection<ProjectDataModel> _projectsCollection;
        private ObservableCollection<UserDataModel> _usersCollection;
        //Public fielads
        public UserToProjectDataModel UserToProject { get; set; }
        public ObservableCollection<ProjectDataModel> ProjectsCollection
        {
            get { return _projectsCollection; }
            set
            {
                _projectsCollection = value;
                OnPropertyChanged();
            }
        }
        public UserDataModel User { get; set; }
        public UserDataModel UserToAdd { get; set; }
        public ProjectDataModel Project
        {
            get { return _project; }
            set
            {
                _project = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<UserDataModel> UsersCollection
        {
            get { return _usersCollection; }
            set
            {
                _usersCollection = value;
                OnPropertyChanged();
            }
        }
        public ProjectDataModel SelectedProject
        {
            get { return _selectedProject; }
            set
            {
                _selectedProject = value;
                OnPropertyChanged();
                ViewUsersMethod();
                ViewManager.Instance.Project = _selectedProject;
            }
        }
        public UserDataModel SelectedUser
        {
            get { return _selectedUser; }
            set
            {
                _selectedUser = value;
                OnPropertyChanged();
            }
        }
        #endregion Properties

        #region Methods
        private void GetAllProjectMethod()
        {
            try
            {
                ProjectsCollection = Service.GetAllProjectMethod(User);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }
        private void AddUserMethod()
        {
            string message;
            message = Service.AddUserMethod(UserToAdd, _selectedProject, User);
            ViewUsersMethod();
            ShowMessage(message);

        }
        private void RemoveUserMethod()
        {
            string message;
            message = Service.RemoveUserMethod(_selectedUser, _selectedProject, User);
            ViewUsersMethod();
            ShowMessage(message);
        }
        private void ViewUsersMethod()
        {
            if (SelectedProject != null)
            {
                try
                {
                    UsersCollection = Service.ViewUsersMethod(SelectedProject);
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message);
                }
            }
        }
        private void AddProjectMethod()
        {
            if (Project.IsValid)
            {
            Service.AddProjectMethod(Project, UserToProject);
            ShowMessage("Project '" + Project.Name + "' is saved.");
            GetAllProjectMethod();
            }
            else
                ShowMessage("Check your input values");
        }

        private void DeleteProjectMethod()
        {
            string message;
            message = Service.DeleteProjectMethod(_selectedProject, User);
            ShowMessage(message);
            GetAllProjectMethod();
        }
        #endregion Methods
    }
}
