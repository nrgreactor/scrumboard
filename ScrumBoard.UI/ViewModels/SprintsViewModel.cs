﻿using ScrumBoard.BLL.Services;
using ScrumBoard.DM;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace ScrumBoard.UI.ViewModels
{
    class SprintsViewModel : PropertyChangedBase
    {
        #region Constructor
        public SprintsViewModel(ProjectDataModel _project, UserDataModel User)
        {
            ViewAllTasksCommand = new Command(arg => ViewAllTasksMethod());
            AddTaskCommand = new Command(arg => AddTaskMethod());
            RemoveTaskCommand = new Command(arg => RemoveTaskMethod());
            ViewSprintTasksCommand = new Command(arg => ViewSprintTasksMethod());
            ViewAllSprintsCommand = new Command(arg => ViewAllSprintsMethod());
            AddSprintCommand = new Command(arg => AddSprintMethod());
            SetSprintDateCommand = new Command(arg => SetSprintDateMethod());
            DeleteSprintCommand = new Command(arg => DeleteSprintMethod());
            CurrentProject = _project;
            CurrentUser = User;
            Service = new SprintsService();
            Sprint = new SprintDataModel() { Name = "New Sprint" };
            SprintsCollection = new ObservableCollection<SprintDataModel>();
            ViewAllSprintsMethod();

        }
        #endregion Constructor

        #region Command
        public ICommand ViewAllTasksCommand { get; set; }
        public ICommand AddTaskCommand { get; set; }
        public ICommand RemoveTaskCommand { get; set; }
        public ICommand ViewSprintTasksCommand { get; set; }
        public ICommand ViewAllSprintsCommand { get; set; }
        public ICommand AddSprintCommand { get; set; }
        public ICommand SetSprintDateCommand { get; set; }
        public ICommand DeleteSprintCommand { get; set; }

        #endregion Command

        #region Properties
        private SprintDataModel CurrentSprint;
        private UserDataModel CurrentUser;

        public SprintDataModel SelectedSprint
        {
            get { return CurrentSprint; }
            set
            {
                CurrentSprint = value;
                OnPropertyChanged();
                ViewSprintTasksMethod();
                ViewAllTasksMethod();
            }
        }

        private ProjectDataModel CurrentProject;

        private ObservableCollection<SprintDataModel> _sprintsCollection;

        public ObservableCollection<SprintDataModel> SprintsCollection
        {
            get { return _sprintsCollection; }
            set
            {
                _sprintsCollection = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<TaskDataModel> _ProjectTasksCollection { get; set; }

        public ObservableCollection<TaskDataModel> ProjectTasksCollection
        {
            get { return _ProjectTasksCollection; }
            set
            {
                _ProjectTasksCollection = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<TaskDataModel> _SprintTasksCollection { get; set; }

        public ObservableCollection<TaskDataModel> SprintTasksCollection
        {
            get { return _SprintTasksCollection; }
            set
            {
                _SprintTasksCollection = value;
                OnPropertyChanged();
            }
        }

        public SprintsService Service;

        private TaskDataModel _selectedTask;
        public TaskDataModel SelectedTask
        {
            get { return _selectedTask; }
            set
            {
                _selectedTask = value;
                OnPropertyChanged();
            }
        }

        private SprintDataModel _sprint;


        public SprintDataModel Sprint
        {
            get { return _sprint; }
            set
            {
                _sprint = value;
                OnPropertyChanged();
            }
        }

        #endregion Properties

        #region Methods
        private void ViewAllTasksMethod()
        {
            try
            {
                ProjectTasksCollection = Service.ViewAllTasksMethod(CurrentProject);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }
        private void AddTaskMethod()
        {
            try
            {
                Service.AddTaskMethod(CurrentSprint, SelectedTask);
                ViewAllTasksMethod();
                ViewSprintTasksMethod();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        private void RemoveTaskMethod()
        {
            try
            {
                Service.RemoveTaskMethod(_selectedTask);
                ViewAllTasksMethod();
                ViewSprintTasksMethod();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        private void ViewSprintTasksMethod()
        {
            try
            {
                SprintTasksCollection = Service.ViewSprintTasksMethod(CurrentSprint);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        private void ViewAllSprintsMethod()
        {
            try
            {
                SprintsCollection = Service.ViewAllSprintsMethod(CurrentProject);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }

        }

        private void AddSprintMethod()
        {
            if (Sprint.IsValid)
            {
                try
                {
                    Service.AddSprintMethod(_sprint, CurrentProject);
                    ViewAllSprintsMethod();
                    ShowMessage("Sprint " + _sprint.Name.Trim() + " was created");
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message);
                }
            }
            else
                ShowMessage("Check your input values");
        }

        private void SetSprintDateMethod()
        {
            if (CurrentSprint != null)
            {
                try
                {
                    Service.SetSprintDateMethod(CurrentSprint);
                    ShowMessage("Sprint " + CurrentSprint.Name.Trim() + " was updated");
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message);
                }
            }
            else
                ShowMessage("Choose sprint");
        }

        private void DeleteSprintMethod()
        {
            string message;
            message = Service.DeleteSprintMethod(CurrentProject, CurrentUser, CurrentSprint);
            ShowMessage(message);
            ViewAllSprintsMethod();
        }
        #endregion Methods
    }
}
