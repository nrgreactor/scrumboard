﻿using ScrumBoard.BLL.Services;
using ScrumBoard.DM;
using ScrumBoard.UI.Navigation;
using ScrumBoard.UI.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;

namespace ScrumBoard.UI.ViewModels
{
    public class UserSettingsViewModel : PropertyChangedBase
    {
        public UserSettingsViewModel()
        {            
            LoadImageCommand = new Command(arg => LoadImageMethod());
            UpdateUserCommand = new Command(arg => UpdateUserMethod());
            LoadUser();
        }
        public ICommand LoadImageCommand { get; set; }
        public ICommand UpdateUserCommand { get; set; }
        private string _login;
        private string _firstName;
        private string _lastName;
        private ImageSource _photoImage;
        private string _email;
        private string _password;
        private byte[] _photo;

        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                OnPropertyChanged();
            }
        }
        public string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                OnPropertyChanged();
            }
        }
        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                OnPropertyChanged();
            }
        }
        public ImageSource PhotoImage
        {
            get { return _photoImage; }
            set
            {
                _photoImage = value;
                OnPropertyChanged();
            }
        }
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                OnPropertyChanged();
            }
        }
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        private void UpdateUserMethod()
        {
            try
            {
                var service = new UserSettingsService();
                var user = ViewManager.Instance.User;
                user.FirstName = this.FirstName;
                user.LastName = this.LastName;
                user.Photo = this._photo;
                user.Email = this.Email;
                if (this.Password.Length > 0)
                {
                    var s = new LogInService();
                    user.Password = s.GetHashString(this.Password);
                }
                service.Update(user);
                ViewManager.Instance.User = user;
                ShowMessage("User settings updated");
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }
        private void LoadUser()
        {
            var user=ViewManager.Instance.User;
            this.Login = user.Login;
            this.FirstName=user.FirstName;
            this.LastName=user.LastName;
            this.Email = user.Email;
            _photo = user.Photo; 
            if(_photo.Length!=0)
            UpdateImage();
        }
        private void LoadImageMethod()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpg";
            dlg.Filter = "All Images |*.jpg;*.jpeg;*.png;*.gif|JPG Files (*.jpg)|*.jpg|JPEG Files (*.jpeg)|*jpeg|PNG Files (*.png)|*.png|GIF Files (*.gif)|*.gif";


            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = @dlg.FileName;
                this._photo = File.ReadAllBytes(@filename);
            }
            UpdateImage();
        }
        private void UpdateImage()
        {
            if (_photo != null)
            {
                ImageSourceConverter converter = new ImageSourceConverter();
                ImageSource imageSource = (ImageSource)converter.ConvertFrom(_photo);
                PhotoImage = imageSource;
            }
        }

    }
}
