﻿using Framework.UI.Controls;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ScrumBoard.UI.ViewModels
{

    public abstract class PropertyChangedBase : INotifyPropertyChanged
    {
        public void OnPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void ShowMessage(string message)
        {
            MessageDialog.ShowAsync(message, null);
        }
    }
}
