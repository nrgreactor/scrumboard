﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumBoard.UI.ViewModels
{
    public static class ConstField
    {
        public  const int TODO = 0;
        public  const int INPR = 1;
        public  const int TEST = 2;
        public  const int DONE = 3;

        public  const int STORY = 0;
        public  const int TASK = 1;
        public  const int BUG = 2;
        public  const int FUTURE = 3;
        public  const int OTHER = 4;
    }
}
