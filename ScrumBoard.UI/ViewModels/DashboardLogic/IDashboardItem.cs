﻿using ScrumBoard.DM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumBoard.UI.ViewModels
{
    public interface IDashboardItem
    {
         TaskDataModel Item { get; set; }
    }
}
