﻿using ScrumBoard.BLL.Services;
using ScrumBoard.DM;
using ScrumBoard.UI.Navigation;
using ScrumBoard.UI.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ScrumBoard.UI.ViewModels
{
    public class DashboardViewModel : PropertyChangedBase
    {
        public DashboardViewModel(ProjectDataModel project)
        {
            _currentProject = project;
            LoadSprints();
            Dashboard = new ObservableCollection<IDashboardItem>();
            NextCommand = new Command(arg => NextMethod());
            PrevCommand = new Command(arg => PrevMethod());
        }

        //Services
        private SprintsService _sService = new SprintsService();
        private DashboardService _dService = new DashboardService();
        //Commands
        public ICommand NextCommand { get; set; }
        public ICommand PrevCommand { get; set; }
       
        #region Private fields
        private ObservableCollection<SprintDataModel> _sprintsCollection;
        private ObservableCollection<TaskDataModel> _tasks;
        private ProjectDataModel _currentProject;
        private SprintDataModel _selectedSprint;
        private ObservableCollection<IDashboardItem> _dashboard;
        private bool _isSelected;
        #endregion
        #region Public fields
        public ObservableCollection<SprintDataModel> SprintsCollection
        {
            get { return _sprintsCollection; }
            set
            {
                _sprintsCollection = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<IDashboardItem> Dashboard
        {
            get { return _dashboard; }
            set
            {
                _dashboard = value;
                GetFromBoard();
                OnPropertyChanged();
            }
        }
        public IDashboardItem SelectedDashboardItem
        {
            set
            {
                SelectedTask = value.Item;
                OnPropertyChanged();
            }
        }
        public SprintDataModel SelectedSprint
        {
            get { return _selectedSprint; }
            set
            {
                _selectedSprint = value;
                OnPropertyChanged();
                LoadTasks();
            }
        }
        public TaskDataModel SelectedTask
        {
            get { return StaticSelectedItem.SelectedTask; }
            set
            {
                StaticSelectedItem.SelectedTask = value;
                OnPropertyChanged();
            }
        }
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged();
                    if (_isSelected)
                    {
                        SelectedDashboardItem = (IDashboardItem)this;
                    }
                }
            }
        }
        #endregion
        
        private void NextMethod()
        {
            if (ChackToDo())
                if (SelectedTask.Status < ConstField.DONE && SelectedTask.Type != ConstField.STORY)
                {
                    SelectedTask.Status++;
                    _dService.SetSprintTask(SelectedTask);
                    LoadTasks();
                }
                else ShowMessage("Task Done");
        }
        private void PrevMethod()
        {
            if (ChackToDo())
                if (SelectedTask.Status > ConstField.TODO && SelectedTask.Type != ConstField.STORY)
                {
                    SelectedTask.Status--;
                    _dService.SetSprintTask(SelectedTask);
                    LoadTasks();
                }
        }
        private void LoadTasks()
        {
            //try
            //{
            _tasks = _dService.GetSprintTasks(SelectedSprint);
            SetToBoard();
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage(ex.Message);
            //}
        }
        private void LoadSprints()
        {
            try
            {
                SprintsCollection = _sService.ViewAllSprintsMethod(_currentProject);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        private void GetFromBoard()
        {
            for (var i = 0; i < Dashboard.Count; i++)
            {
                _tasks[i] = Dashboard[i].Item;
            }
        }
        private void SetToBoard()
        {
            Dashboard = new ObservableCollection<IDashboardItem>();
            foreach (var item in _tasks)
            {
                if (item.Type == ConstField.STORY)
                {
                    var story = new StoryDashboardItem(item, GetStoryTasks(item));
                    Dashboard.Add(story);
                }
                else if (item.StoryId == null)
                {
                    Dashboard.Add(new DashboardItem(item));
                }
            }
        }
        private bool ChackToDo()
        {
            if (SelectedTask != null && SelectedTask.UserId==ViewManager.Instance.User.Id)
                return true;
            else return false;
        }

        private ObservableCollection<TaskDataModel> GetStoryTasks(TaskDataModel task)
        {
             var temp1=new ObservableCollection<TaskDataModel>();
             var temp2 = from item in _tasks
                         where item.StoryId == task.Id
                         select item;
            foreach(var item in temp2)
            {
                temp1.Add(item);
            }
            return temp1;
        }
    }

}

