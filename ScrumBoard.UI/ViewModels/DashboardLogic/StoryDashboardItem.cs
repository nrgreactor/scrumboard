﻿using ScrumBoard.DM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumBoard.UI.ViewModels
{
    class StoryDashboardItem :PropertyChangedBase,IDashboardItem
    {
        public StoryDashboardItem(TaskDataModel task, ObservableCollection<TaskDataModel> items)
        {            
            Items=new ObservableCollection<DashboardItem>();
            this.Item = task;
            foreach (var i in items)
            {
                this.Items.Add(new DashboardItem(i));
            }
        }
        public IDashboardItem SelectedDashboardItem
        {
            set
            {
                StaticSelectedItem.SelectedTask = value.Item;
                OnPropertyChanged();
            }
        }

        private TaskDataModel _task;
        public TaskDataModel Item
        {
            get
            {
                return _task;
            }
            set
            {
                _task = value;
                OnPropertyChanged();
            }
        }
        private ObservableCollection<DashboardItem> _items;
        public ObservableCollection<DashboardItem> Items
        {
            get { return _items; }
            set
            {
                _items = value;
                OnPropertyChanged();
            }
        }

    }
}
