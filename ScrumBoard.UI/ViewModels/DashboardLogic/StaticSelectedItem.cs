﻿using ScrumBoard.DM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumBoard.UI.ViewModels
{
    public static class StaticSelectedItem
    {
        public static TaskDataModel SelectedTask { get; set; }
    }
}
