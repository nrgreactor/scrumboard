﻿using ScrumBoard.DM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumBoard.UI.ViewModels
{
    public class DashboardItem:IDashboardItem
    {

        public DashboardItem(TaskDataModel task)
        {
            this.Item = task;
        }

        private TaskDataModel _task;
        public bool todo { get { if (_task.Status == ConstField.TODO)return true; else return false; } set { } }
        public bool inprogress { get { if (_task.Status == ConstField.INPR)return true; else return false; } set { } }
        public bool testing { get { if (_task.Status == ConstField.TEST)return true; else return false; } set { } }
        public bool done { get { if (_task.Status == ConstField.DONE)return true; else return false; } set { } }

        public TaskDataModel ToDo
        {
            get
            {
                if (_task.Status == ConstField.TODO) return _task;
                else return null;
            }
        }
        public TaskDataModel InProgress
        {
            get
            {
                if (_task.Status == ConstField.INPR) return _task;
                else return null;
            }
        }
        public TaskDataModel Testing
        {
            get
            {
                if (_task.Status == ConstField.TEST) return _task;
                else return null;
            }
        }
        public TaskDataModel Done
        {
            get
            {
                if (_task.Status == ConstField.DONE) return _task;
                else return null;
            }
        }

        public TaskDataModel Item
        {
            get
            {
                return _task;
            }
            set
            {
                _task = value;                
            }
        }


    }
}
