﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScrumBoard.DM;
using System.Windows.Input;
using ScrumBoard.BLL.Services;
using Framework.UI.Controls;
using System.Collections.ObjectModel;
using ScrumBoard.UI.Navigation;
using ScrumBoard.UI.Views;

namespace ScrumBoard.UI.ViewModels
{
    public class BacklogViewModel : PropertyChangedBase
    {
        public BacklogViewModel(ProjectDataModel project, UserDataModel user)
        {
            SomeTask = new TaskDataModel() { Type = ConstField.TASK, Name = "New Task", Status = ConstField.TODO };
            _currentProject = project;
            AddTaskCommand = new Command(arg => AddTaskMethod());
            UpdateTaskCommand = new Command(arg => UpdateTaskMethod());
            DeleteTaskCommand = new Command(arg => DeleteTaskMethod());
            ClearTaskCommand = new Command(arg => ClearTaskMethod());
            AddSubtaskCommand = new Command(arg => AddSubtaskMethod());
            DeleteSubtaskCommand = new Command(arg => DeleteSubtaskMethod());
            Tasks = new ObservableCollection<TaskDataModel>();

            GetUsers();
            GetTasksMethod();

        }
        //Services
        private BacklogService s = new BacklogService();
        //Commands
        public ICommand AddTaskCommand { get; set; }
        public ICommand UpdateTaskCommand { get; set; }
        public ICommand DeleteTaskCommand { get; set; }
        public ICommand ClearTaskCommand { get; set; }
        public ICommand AddSubtaskCommand { get; set; }
        public ICommand DeleteSubtaskCommand { get; set; }


        #region Fields
        private ObservableCollection<UserDataModel> _projectUsers;
        private TaskDataModel _someTask;
        private TaskDataModel _seletedTask;
        private ProjectDataModel _currentProject;
        private ObservableCollection<TaskDataModel> _tasks;
        private ObservableCollection<TaskDataModel> _subtasks;
        private TaskDataModel _selectedSubtask;
        private bool _storyInicator;

        public bool StoryInicator
        {
            get { return _storyInicator; }
            set
            {
                _storyInicator = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<TaskDataModel> Subtasks
        {
            get { return _subtasks; }
            set
            {
                _subtasks = value;
                OnPropertyChanged();
            }
        }
        public TaskDataModel SelectedSubtask
        {
            get
            {
                return _selectedSubtask;
            }
            set
            {
                _selectedSubtask = value;
                OnPropertyChanged();
            }
        }

        public int _selectedUserIndex;
        public ObservableCollection<UserDataModel> ProjectUsers
        {
            get { return _projectUsers; }
            set
            {
                _projectUsers = value;
                OnPropertyChanged();
            }
        }
        public TaskDataModel SomeTask
        {
            get { return _someTask; }
            set
            {
                _someTask = value;
                OnPropertyChanged();
            }
        }
        public TaskDataModel SelectedTask
        {
            get
            {
                return _seletedTask;
            }
            set
            {
                _seletedTask = value;
                OnPropertyChanged("SelectedTask");
                SelectedUserIndex = GetIndex();
                GetSubtasks();
                if (_seletedTask.Type == ConstField.STORY) StoryInicator = true;
                else StoryInicator = false;
            }
        }

        public int SelectedUserIndex
        {
            get { return _selectedUserIndex; }
            set
            {
                _selectedUserIndex = value;
                OnPropertyChanged("SelectedUserIndex");
            }
        }



        public ObservableCollection<TaskDataModel> Tasks
        {
            get { return _tasks; }
            set
            {
                _tasks = value;
                OnPropertyChanged();
            }
        }
        #endregion
        //private methods
        private int GetIndex()
        {
            if (SelectedTask.UserId != null)
                return ProjectUsers.IndexOf(ProjectUsers.First(s => s.Id == SelectedTask.UserId));
            else return -1;
        }

        private void ClearTaskMethod()
        {
            SomeTask = new TaskDataModel() { Type = ConstField.TASK, Name = "New Task", Status = 0 };
        }
        private void AddTaskMethod()
        {
            if (SomeTask.IsValid)
            {
                SomeTask.ProjectId = _currentProject.Id;
                if (SomeTask.User != null)
                    SomeTask.UserId = SomeTask.User.Id;
                s.AddTask(SomeTask);
                ShowMessage("Task Added");
                GetTasksMethod();
            }
            else
                ShowMessage("Check your input values");
        }
        private void GetTasksMethod()
        {
            if (_currentProject != null)
            {
                Tasks = s.GetTasks(_currentProject);
                GetSubtasks();
            }
        }
        private void UpdateTaskMethod()
        {
            s.UpdateTask(SelectedTask);
            GetTasksMethod();
            ShowMessage("Task updated");
        }
        private void DeleteTaskMethod()
        {
            if (SelectedTask.UserId == ViewManager.Instance.User.Id)
            {
                s.DeleteTask(SelectedTask);
                GetTasksMethod();
                ShowMessage("Task deleted");
            }
        }
        private void GetUsers()
        {
            ProjectsService s = new ProjectsService();
            ProjectUsers = s.ViewUsersMethod(ViewManager.Instance.Project);
        }

        private void AddSubtaskMethod()
        {
            int tempId = _seletedTask.Id; 
            TaskDataModel SelTask;
            var freeTasks = new List<TaskDataModel>();
            foreach (var item in Tasks)
            {
                if (item.Type != ConstField.STORY && item.StoryId == null)
                    freeTasks.Add(item);
            }
            AddSubtasks w = new AddSubtasks();
            w.AllTasks = freeTasks;
            w.ShowDialog();
            SelTask = w.SelTask;                   
            w.Close();
            if (SelTask != null)
            {
                SelTask.StoryId = tempId;
                s.UpdateTask(SelTask);
                GetTasksMethod();
            }

        }
        private void DeleteSubtaskMethod()
        {
            SelectedSubtask.StoryId = null;
            s.UpdateTask(SelectedSubtask);
            GetTasksMethod();
        }

        
        private void GetSubtasks()
        {
            if (SelectedTask.Type == ConstField.STORY)
            {
                Subtasks = new ObservableCollection<TaskDataModel>();
                foreach (var item in Tasks)
                {
                    if (item.StoryId == SelectedTask.Id)
                        Subtasks.Add(item);
                }
            }
            else Subtasks = new ObservableCollection<TaskDataModel>();
        }
    }
}
