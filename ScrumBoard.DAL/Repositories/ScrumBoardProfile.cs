﻿using AutoMapper;
using System.Collections.Generic;
using ScrumBoard.DM;

namespace ScrumBoard.DAL.Repositories
{
    public class ScrumBoardProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<User, UserDataModel>();
            Mapper.CreateMap<UserDataModel, User>();

            Mapper.CreateMap<Task, TaskDataModel>();
            Mapper.CreateMap<TaskDataModel, Task>();

            Mapper.CreateMap<Sprint, SprintDataModel>();
            Mapper.CreateMap<SprintDataModel, Sprint>();

            Mapper.CreateMap<Project, ProjectDataModel>();
            Mapper.CreateMap<ProjectDataModel, Project>();

            Mapper.CreateMap<UserToProjeсt, UserToProjectDataModel>();
            Mapper.CreateMap<UserToProjectDataModel, UserToProjeсt>();

            Mapper.CreateMap<Message, MessageDataModel>();
            Mapper.CreateMap<MessageDataModel, Message>();
        }       
     }
}