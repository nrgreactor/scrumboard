﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ScrumBoard.DM;

namespace ScrumBoard.DAL.Repositories
{
    public class ProjectRepository :MappedRepository<Project,ProjectDataModel>, IRepository<ProjectDataModel>
    {
        public IEnumerable<ProjectDataModel> GetAll()
        {
            using (var context = new ScrumboardEntities())
            {
                return context.Projects.Select(s => GetMap(s)).ToList<ProjectDataModel>();

            }
        }

        public ProjectDataModel GetByID(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                var s = context.Projects.FirstOrDefault(o => o.Id == id);
                return GetMap(s);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Project id</returns>
        public void Add(ProjectDataModel entity)
        {
            //using  Context object

            using (var context = new ScrumboardEntities())
            {
                //login check for uniqueness
               
                if (!context.Projects.Any(s => s.Id == entity.Id))
                {
                    context.Projects.Add(GetMap(entity));                   
                    context.SaveChanges();
                }
                else throw new EntityAlreadyExistsException();
            }
        }

        public void Update(ProjectDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Projects.Find(entity.Id);
                if (temp != null)
                {
                    temp = GetMap(entity);                        
                    context.Entry(temp).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public void Delete(ProjectDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Projects.Find(entity.Id);
                if (temp != null)
                {
                    context.Projects.Remove(temp);
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }
        public IEnumerable<ProjectDataModel> GetProjectsByUserId(int _id)
        {
            using (var context = new ScrumboardEntities())
            {
                var ProjectsOfUser = from item in context.UserToProjeсt
                        where item.UserId==_id
                        select item;   
                var ProjectsList=new List<ProjectDataModel>();

                foreach (var item in ProjectsOfUser)
                {
                    var project=context.Projects.Find(item.ProjectId);
                    ProjectsList.Add(GetMap(project));
                }
                return ProjectsList;
                }
        }
        public void Add(ProjectDataModel projectEntity, UserToProjectDataModel usToEntity) 
        {
            //using  Context object

            using (var context = new ScrumboardEntities())
            {
                //login check for uniqueness

                if (!context.Projects.Any(s => s.Id == projectEntity.Id))
                {
                    var project = GetMap(projectEntity);
                    
                    var userTo = new UserToProjeсt()
                    {
                        ProjectId = project.Id,
                        UserId = usToEntity.UserId,
                        Role = usToEntity.Role
                    };

                    context.Projects.Add(project);
                    context.UserToProjeсt.Add(userTo);
                    context.SaveChanges();
                }
                else throw new EntityAlreadyExistsException();
            }
        }
    }
}
