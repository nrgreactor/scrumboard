﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ScrumBoard.DM;

namespace ScrumBoard.DAL.Repositories
{
    public class SprintRepository : MappedRepository<Sprint, SprintDataModel>, IRepository<SprintDataModel>
    {
        public IEnumerable<SprintDataModel> GetAll()
        {
            using (var context = new ScrumboardEntities())
            {
                return context.Sprints.Select(s => GetMap(s)).ToList<SprintDataModel>();
            }
        }

        public SprintDataModel GetByID(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                var s = context.Sprints.First(o => o.Id == id);
                return GetMap(s);
            }
        }

        public void Add(SprintDataModel entity)
        {
            //using  Context object
            using (var context = new ScrumboardEntities())
            {
                //login check for uniqueness
                if (!context.Sprints.Any(s => s.Id == entity.Id))
                {
                    context.Sprints.Add(GetMap(entity));
                    context.SaveChanges();
                }
                else throw new EntityAlreadyExistsException();
            }
        }

        public void Update(SprintDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Sprints.Find(entity.Id);
                if (temp != null)
                {
                    temp.Id = entity.Id;
                    temp.Name = entity.Name;
                    temp.ProjectId = entity.ProjectId;
                    temp.DateStart = entity.DateStart;
                    temp.DateEnd = entity.DateEnd;
                    context.Entry(temp).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public void Delete(SprintDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Sprints.Find(entity.Id);
                if (temp != null)
                {
                    context.Sprints.Remove(temp);
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public IEnumerable<SprintDataModel> GetByProjectId(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Sprints.Where(p => p.ProjectId == id);
                if (temp != null)
                {
                    return context.Sprints.Where(p => p.ProjectId == id).Select(s => new SprintDataModel()
                    {
                        Id = s.Id,
                        Name = s.Name,
                        DateStart = (DateTime)s.DateStart,
                        DateEnd = (DateTime)s.DateEnd,
                        ProjectId = s.ProjectId
                    }).ToList<SprintDataModel>();
                }
                else return null;
            }
        }
    }
}
