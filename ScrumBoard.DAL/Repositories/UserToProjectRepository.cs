﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ScrumBoard.DM;

namespace ScrumBoard.DAL.Repositories
{
    public class UserToProjectRepository : MappedRepository<UserToProjeсt, UserToProjectDataModel>, IRepository<UserToProjectDataModel>
    {
        public IEnumerable<UserToProjectDataModel> GetAll()
        {
            using (var context = new ScrumboardEntities())
            {
                return context.UserToProjeсt.Select(s => GetMap(s)).ToList<UserToProjectDataModel>();
            }
        }

        public UserToProjectDataModel GetByID(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                var s = context.UserToProjeсt.First(o => o.Numbers == id);
                return GetMap(s);
            }
        }

        public void Add(UserToProjectDataModel entity)
        {
            //using  Context object
            using (var context = new ScrumboardEntities())
            {
                //login check for uniqueness
                if (!context.UserToProjeсt.Any(s => s.Numbers == entity.Numbers))
                {
                    context.UserToProjeсt.Add(GetMap(entity));
                    context.SaveChanges();
                }
                else throw new EntityAlreadyExistsException();
            }
        }

        public void Update(UserToProjectDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.UserToProjeсt.Find(entity.Numbers);
                if (temp != null)
                {
                    temp = GetMap(entity);

                    context.Entry(temp).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public void Delete(UserToProjectDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.UserToProjeсt.Find(entity.Numbers);
                if (temp != null)
                {
                    context.UserToProjeсt.Remove(temp);
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public void Delete(ProjectDataModel project, UserDataModel user)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.UserToProjeсt.Where(p => p.UserId == user.Id).Where(s => s.ProjectId == project.Id).First();              
                if (temp != null)
                {
                    context.UserToProjeсt.Remove(temp);
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public UserToProjectDataModel GetByProjectId(UserDataModel user, ProjectDataModel project)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.UserToProjeсt.Where(p => p.UserId == user.Id).Where(s => s.ProjectId == project.Id).First();
                return GetMap(temp);
                    
            }  
        }
    }
}
