﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace ScrumBoard.DAL.Repositories
{
    public class MappedRepository<TEntity,TModel> : ScrumBoardProfile
    {
        public MappedRepository()
        {
            this.Configure();
        }
        protected TModel GetMap(TEntity entity)
        {
            return Mapper.Map<TEntity, TModel>(entity);
        }
        protected TEntity GetMap(TModel model)
        {
            return Mapper.Map<TModel, TEntity>(model);
        }
    }
}
