﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ScrumBoard.DM;

namespace ScrumBoard.DAL.Repositories
{
    public class UserRepository : MappedRepository<User,UserDataModel>, IRepository<UserDataModel>
    {
        public IEnumerable<UserDataModel> GetAll()
        {
            using (var context = new ScrumboardEntities())
            {
                var list = new List<UserDataModel>();
                foreach(var item in context.Users.ToList())
                {
                    list.Add(GetMap(item));
                }
                return list;
            }

        }

        public UserDataModel GetByID(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                var s = context.Users.First(o => o.Id == id);
                return GetMap(s);
            }
        }

        public void Add(UserDataModel entity)
        {
            //using  Context object
            using (var context = new ScrumboardEntities())
            {
                //login check for uniqueness
                if (!context.Users.Any(s => s.Login == entity.Login))
                {
                    context.Users.Add(GetMap(entity));
                    context.SaveChanges();
                }
                else throw new EntityAlreadyExistsException("This nickname are used");
            }
        }

        public void Update(UserDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Users.Find(entity.Id);
                if (temp != null)
                {
                    temp.Email = entity.Email;
                    temp.FirstName = entity.FirstName;                    
                    temp.LastName = entity.LastName;
                    temp.Password = entity.Password;
                    temp.Photo = entity.Photo;                    
                    context.Entry(temp).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found");
            }
        }

        public void Delete(UserDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                try
                {
                    var temp = context.Users.Find(entity.Id);
                    context.Users.Remove(temp);
                    context.SaveChanges();
                }
                catch { throw new EntityNotFoundException("User not found"); }
            }
        }
        public UserDataModel GetByNickname(string _nickname)
        {
            using (var context = new ScrumboardEntities())
            {
                try
                {
                    var s = context.Users.First(o => o.Login == _nickname);
                    if (s != null)
                        return GetMap(s);
                    else return null; 
                    //else throw new EntityNotFoundException("Nickname not found.");
                }
                catch { throw new EntityNotFoundException("Nickname not found"); }
            }
        }
        public IEnumerable<UserDataModel> GetByProjectID(int _id)
        {
            using (var context = new ScrumboardEntities())
            {
                //обьявление обьекта - списка елементов таблицы User_to_Project по указанному _id некоторого User-а
                var UsersOfProject = from item in context.UserToProjeсt
                                     where item.ProjectId == _id
                                     select item;
                var UsersList = new List<UserDataModel>();
                foreach (var item in UsersOfProject)
                {
                    var user = context.Users.Find(item.UserId);
                    UsersList.Add(GetMap(user));
                }
                return UsersList;
            }
        }
    }
}
