﻿using ScrumBoard.DM;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumBoard.DAL.Repositories
{
    public class MessageRepository : MappedRepository<Message, MessageDataModel>, IRepository<MessageDataModel>
    {
        public IEnumerable<MessageDataModel> GetAll()
        {
            using (var context = new ScrumboardEntities())
            {
                return context.Messages.Select(s => GetMap(s)).ToList<MessageDataModel>();

            }
        }

        public MessageDataModel GetByID(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                var s = context.Messages.FirstOrDefault(o => o.Id == id);
                return GetMap(s);
            }
        }

        public void Add(MessageDataModel entity)
        {
            //using  Context object

            using (var context = new ScrumboardEntities())
            {
                //login check for uniqueness

                if (!context.Messages.Any(s => s.Id == entity.Id))
                {
                    context.Messages.Add(GetMap(entity));
                    context.SaveChanges();
                }
                else throw new EntityAlreadyExistsException();
            }
        }

        public void Update(MessageDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Messages.Find(entity.Id);
                if (temp != null)
                {
                    temp.Description = entity.Description;
                    temp.TaskId = entity.TaskId;
                    temp.UserId = entity.UserId;
                    temp.Time = entity.Time;
                    context.Entry(temp).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public void Delete(MessageDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Messages.Find(entity.Id);
                if (temp != null)
                {
                    context.Messages.Remove(temp);
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public IEnumerable<MessageDataModel> GetMessagesByTaskId(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                IEnumerable<MessageDataModel> result = (from us in context.Users
                                                    join ms in context.Messages on us.Id equals ms.UserId
                                                    where ms.TaskId == id
                                                        select GetMap(ms)).ToList<MessageDataModel>();
                return result;
            }

        }
    }
}
