﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ScrumBoard.DM;

namespace ScrumBoard.DAL.Repositories
{
    public class TaskRepository : MappedRepository<Task, TaskDataModel>, IRepository<TaskDataModel>
    {
       
        public IEnumerable<TaskDataModel> GetAll()
        {
            using (var context = new ScrumboardEntities())
            {
                return context.Tasks.Select(s => GetMap(s)).ToList<TaskDataModel>();
            }
        }

        public TaskDataModel GetByID(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                var s = context.Tasks.First(o => o.Id == id);
                return GetMap(s);
            }
        }

        public void Add(TaskDataModel entity)
        {
            //using  Context object
            using (var context = new ScrumboardEntities())
            {
                //id check for uniqueness
                if (!context.Tasks.Any(s => s.Id == entity.Id))
                {
                    context.Tasks.Add(GetMap(entity));

                    context.SaveChanges();
                }
                else throw new EntityAlreadyExistsException();
            }
        }

        public void Update(TaskDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Tasks.Find(entity.Id);
                if (temp != null)
                {
                    temp.Descriprion = entity.Descriprion;
                    temp.DateStart = entity.DateStart;
                    temp.DateEnd = entity.DateEnd;
                    temp.Estimate = entity.Estimate;
                    temp.Name = entity.Name.Trim();
                    temp.Status = entity.Status;
                    temp.Type = entity.Type;
                    temp.UserId = entity.UserId;
                    temp.ProjectId = entity.ProjectId;
                    temp.SprintId = entity.SprintId;
                    temp.Time = entity.Time;
                    temp.StoryId = entity.StoryId;
                    //temp.User = entity.User;
                    context.Entry(temp).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }

        public void Delete(TaskDataModel entity)
        {
            using (var context = new ScrumboardEntities())
            {
                var temp = context.Tasks.Find(entity.Id);
                if (temp != null)
                {
                    context.Tasks.Remove(temp);
                    context.SaveChanges();
                }
                else throw new EntityNotFoundException("Entity not found.");
            }
        }
        public IEnumerable<TaskDataModel> GetByProjectId(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                List<TaskDataModel> list=new List<TaskDataModel>();
                foreach (var item in context.Tasks.Where(p => p.ProjectId == id).ToList<Task>())
                {
                    list.Add(GetMap(item));
                }
                return list;
            }
        }
        public IEnumerable<TaskDataModel> GetBySprintId(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                List<TaskDataModel> list = new List<TaskDataModel>();
                foreach (var item in context.Tasks.Where(p => p.SprintId == id).ToList<Task>())
                {
                    list.Add(GetMap(item));
                }
                return list;
            }
        }
        public IEnumerable<TaskDataModel> GetByStoryId(int id)
        {
            using (var context = new ScrumboardEntities())
            {
                List<TaskDataModel> list = new List<TaskDataModel>();
                foreach (var item in context.Tasks.Where(p => p.StoryId == id).ToList<Task>())
                {
                    list.Add(GetMap(item));
                }
                return list;
            }
        }
    }
}
