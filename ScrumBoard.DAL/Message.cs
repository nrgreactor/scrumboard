//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ScrumBoard.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Message
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Time { get; set; }
        public int TaskId { get; set; }
        public int UserId { get; set; }
    
        public virtual Task Task { get; set; }
        public virtual User User { get; set; }
    }
}
