//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ScrumBoard.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class User
    {
        public User()
        {
            this.Messages = new HashSet<Message>();
            this.Tasks = new HashSet<Task>();
            this.UserToProjeсt = new HashSet<UserToProjeсt>();
        }
    
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public byte[] Photo { get; set; }
        public string Email { get; set; }
    
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
        public virtual ICollection<UserToProjeсt> UserToProjeсt { get; set; }
    }
}
